﻿using UnityEngine;
using System.Collections;

public class AttackBottom : MonoBehaviour {
	float bTime;
	float bAttackX;
	float bRandomX;
	float bAttackY;
	Vector3 bPos;
	
	// Update is called once per frame
	void Update () {
		bTime = bTime + 1;
		if(bTime == 150) {
			bAttackY = 0;
			bRandomX = Random.Range(5, -5);
			bAttackX = bRandomX;
			bPos = new Vector3(bAttackX, bAttackY);
			transform.position = bPos;
		}
		if(bTime == 200) {
			bAttackY = 0;
			bAttackX = 15;
			bPos = new Vector3(bAttackX, bAttackY);
			transform.position = bPos;
			bTime = 0;
		}
	}
}

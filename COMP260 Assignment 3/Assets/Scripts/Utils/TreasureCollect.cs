﻿using UnityEngine;
using System.Collections;

public class TreasureCollect : MonoBehaviour {
	float x;
	float y;
	Vector3 pos;

	
	void start() {
		x = Random.Range(10, -10);
		y = Random.Range(4, -4);
		pos = new Vector3(x, y);
		transform.position = pos;
	}
	
	void OnCollisionEnter2D(Collision2D collision) {
		x = Random.Range(6, -6);
		y = Random.Range(4, -4);
		pos = new Vector3(x, y);
		transform.position = pos;
	}
}
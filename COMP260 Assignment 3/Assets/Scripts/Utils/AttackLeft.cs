﻿using UnityEngine;
using System.Collections;

public class AttackLeft : MonoBehaviour {
	float lTime;
	float lAttackX;
	float lRandomY;
	float lAttackY;
	Vector3 lPos;
	
	// Update is called once per frame
	void Update () {
		lTime = lTime + 1;
		if(lTime == 110) {
			lAttackX = 0;
			lRandomY = Random.Range(5, -5);
			lAttackY = lRandomY;
			lPos = new Vector3(lAttackX, lAttackY);
			transform.position = lPos;
		}
		if(lTime == 160) {
			lAttackX = 0;
			lAttackY = 15;
			lPos = new Vector3(lAttackX, lAttackY);
			transform.position = lPos;
			lTime = 0;
		}
	}
}

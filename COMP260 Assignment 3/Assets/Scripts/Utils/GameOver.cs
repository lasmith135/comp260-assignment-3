﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {
	Vector3 gameOver;
	
	void OnCollisionEnter2D(Collision2D collision) {
		if(collision.gameObject.tag == "Enemy"){
		gameOver = new Vector3(10000, 10000);
		transform.position = gameOver;
		}
	}
}

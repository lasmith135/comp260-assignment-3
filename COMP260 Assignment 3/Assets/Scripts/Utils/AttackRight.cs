﻿using UnityEngine;
using System.Collections;

public class AttackRight : MonoBehaviour {
	float rTime;
	float rAttackX;
	float rRandomY;
	float rAttackY;
	Vector3 rPos;
	
	// Update is called once per frame
	void Update () {
		rTime = rTime + 1;
		if(rTime == 180) {
			rAttackX = 0;
			rRandomY = Random.Range(5, -5);
			rAttackY = rRandomY;
			rPos = new Vector3(rAttackX, rAttackY);
			transform.position = rPos;
		}
		if(rTime == 230) {
			rAttackX = 0;
			rAttackY = 15;
			rPos = new Vector3(rAttackX, rAttackY);
			transform.position = rPos;
			rTime = 0;
		}
	}
}

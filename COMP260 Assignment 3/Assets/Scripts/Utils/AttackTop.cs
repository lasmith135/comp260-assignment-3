﻿using UnityEngine;
using System.Collections;

public class AttackTop : MonoBehaviour {
	float tTime;
	float tAttackX;
	float tRandomX;
	float tAttackY;
	Vector3 tPos;
	
	// Update is called once per frame
	void Update () {
		tTime = tTime + 1;
		if(tTime == 130) {
			tAttackY = 0;
			tRandomX = Random.Range(5, -5);
			tAttackX = tRandomX;
			tPos = new Vector3(tAttackX,tAttackY);
			transform.position = tPos;
		}
		if(tTime == 180) {
			tAttackY = 0;
			tAttackX = 15;
			tPos = new Vector3(tAttackX, tAttackY);
			transform.position = tPos;
			tTime = 0;
		}
	}
}
